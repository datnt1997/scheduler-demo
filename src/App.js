import React, { Component } from 'react';

import './App.css';

import Dnd from './Dnd';
class App extends Component {
  render() {
    return (
      <div className="App">
        <Dnd />
      </div>
    );
  }
}

export default App;
