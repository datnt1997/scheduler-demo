import React from 'react';
import events from './events';
import BigCalendar from 'react-big-calendar';
import Toolbar from './Toolbar';
// import EventComponent from './EventComponent';
import TimeGutterHeader from './TimeGutterHeader';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import moment from 'moment';
import filter from 'lodash/filter';
import difference from 'lodash/difference';

import 'react-big-calendar/lib/css/react-big-calendar.css';

import 'react-big-calendar/lib/addons/dragAndDrop/styles.css';
import './style.css';

const DragAndDropCalendar = withDragAndDrop(BigCalendar)

// let resources = [
//     {
//         resourceId: 1,
//         resourceTitle: 'Course',
//     },
//     {
//         resourceId: 2,
//         resourceTitle: 'Units',
//     },
// ]

class Dnd extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            events: events,
        }
    }

    componentDidMount() {
        const { events } = this.state;
        if (events && events.length > 0) {
            const aloneUnitEventsArray = events.filter(item => {
                const courseEventInPeriod = filter(events, (event) => {
                    if ((event.resource === 1 && (moment(event.start).isSame(item.start) || moment(event.end).isSame(item.end)))
                        || (moment(event.start).isBetween(item.start, item.end) && moment(event.end).isBetween(item.start, item.end))
                    ) {
                        return true;
                    }
                    return false;
                });
                if (item.resource === 2 && courseEventInPeriod.length === 0) {
                    return true;
                }
                return false;
            });

            if (aloneUnitEventsArray.length > 0) {
                let nextEvents = [...events];
                aloneUnitEventsArray.forEach(event => {
                    const newEvent = {
                        id: event.id,
                        title: 'New Event',
                        allDay: true,
                        start: event.start,
                        end: event.end,
                        resource: 3,
                    }
                    const id = event.id + 1;
                    const newUpdatedEvent = { ...event, id }
                    nextEvents.splice(event.id, 1, newEvent, newUpdatedEvent);
                    nextEvents = this.updateIndex(nextEvents);
                })
                this.setState({ events: nextEvents });
            }

        }
    }

    moveEvent = ({ event, start, end, isAllDay: droppedOnAllDaySlot }) => {
        const { events } = this.state

        const idx = events.indexOf(event)
        let allDay = event.allDay

        if (!event.allDay && droppedOnAllDaySlot) {
            allDay = true
        } else if (event.allDay && !droppedOnAllDaySlot) {
            allDay = false
        }

        const updatedEvent = { ...event, start, end, allDay }

        const dayOfWeekStart = moment(start).day();
        const dayOfWeekEnd = moment(end).day();

        if (dayOfWeekStart !== 0 && dayOfWeekStart !== 6 && dayOfWeekEnd !== 0 && dayOfWeekEnd !== 6 && event.resource !== 3) {

            //if event is a unit event
            if (event.resource === 2) {
                //check is the day slot is available or not
                const unavailableDaySlotArray = filter(events, (item) => {
                    /**just check one or two conditions
                     * 1.start or end is the same with item in array
                     * 2.start or end is between the period of item
                     */
                    if ((moment(start).isSame(item.start) || moment(end).isSame(item.end))
                        || (moment(start).isBetween(item.start, item.end) && moment(end).isBetween(item.start, item.end))
                    ) {
                        return true;
                    }
                    return false;
                });
                //if there is not any event in slot, create a new event and add to the array then update index
                if (unavailableDaySlotArray.length === 0) {
                    const nextEvents = [...events];
                    const newEvent = {
                        id: event.id,
                        title: 'New Event',
                        allDay: true,
                        start: start,
                        end: end,
                        resource: 3,
                    }
                    const id = event.id + 1;
                    const newUpdatedEvent = { ...event, start, end, allDay, id }
                    nextEvents.splice(idx, 1, newEvent, newUpdatedEvent);
                    const updatedEvents = this.updateIndex(nextEvents);
                    this.setState({
                        events: updatedEvents,
                    })
                } else {
                    const nextEvents = [...events]
                    nextEvents.splice(idx, 1, updatedEvent)

                    this.setState({
                        events: nextEvents,
                    })
                }

            } else {
                //check is it a course event
                if (event.resource === 1) {
                    //check is there any hidden event in the period of existed array or the same with start and end
                    const unnecessaryEventArray = filter(events, (item) => {
                        if ((item.resource === 3 && (moment(start).isSame(item.start) || moment(end).isSame(item.end)))
                            || (moment(start).isBetween(item.start, item.end) && moment(end).isBetween(item.start, item.end))
                        ) {
                            return true;
                        }
                        return false;
                    });

                    //if there is any hidden event delete it then update the event
                    if (unnecessaryEventArray.length !== 0) {
                        const hiddenItemID = unnecessaryEventArray[0].id;
                        const nextEvents = [...events];
                        nextEvents.splice(hiddenItemID, 1);
                        nextEvents.splice(idx, 1, updatedEvent);
                        const updatedEvents = this.updateIndex(nextEvents);

                        this.setState({
                            events: updatedEvents,
                        })

                    } else {

                        //Find unit events in period of course event before moving 
                        const unitEventsBetweenBefore = events.filter(existingEvent => {
                            return existingEvent.resource === 2
                                && (moment(existingEvent.start).isBetween(event.start, event.end) || moment(existingEvent.start).isSame(event.start))
                                && (moment(existingEvent.end).isBetween(event.start, event.end) || moment(existingEvent.end).isSame(event.end));
                        });

                        //Find unit events in period of course event after moving
                        const unitEventsBetweenAfter = events.filter(existingEvent => {
                            return existingEvent.resource === 2
                                && (moment(existingEvent.start).isBetween(start, end) || moment(existingEvent.start).isSame(start))
                                && (moment(existingEvent.end).isBetween(start, end) || moment(existingEvent.end).isSame(end));
                        });

                        //Compare to find the affected unit event after moving
                        const differentEvents = difference(unitEventsBetweenBefore, unitEventsBetweenAfter);

                        //if there is any affected unit event create a hidden event above it then update event
                        if (differentEvents.length > 0) {
                            const nextEvents = [...events];
                            const newEvent = {
                                id: differentEvents[0].id,
                                title: 'New Event',
                                allDay: true,
                                start: differentEvents[0].start,
                                end: differentEvents[0].end,
                                resource: 3,
                            }
                            const id = differentEvents[0].id + 1;
                            const newUpdatedEvent = { ...differentEvents[0], id }
                            nextEvents.splice(idx, 1, updatedEvent);
                            nextEvents.splice(differentEvents[0].id, 1, newEvent, newUpdatedEvent);
                            const updatedEvents = this.updateIndex(nextEvents);
                            this.setState({
                                events: updatedEvents,
                            })
                        } else {
                            const nextEvents = [...events];
                            nextEvents.splice(idx, 1, updatedEvent);

                            this.setState({
                                events: nextEvents,
                            })
                        }
                    }
                } else {
                    const nextEvents = [...events]
                    nextEvents.splice(idx, 1, updatedEvent)

                    this.setState({
                        events: nextEvents,
                    })
                }
            }

            // alert(`${event.title} was dropped onto ${updatedEvent.start}`)
        }
    }

    resizeEvent = ({ event, start, end }) => {
        const { events } = this.state;

        const dayOfWeekStart = moment(start).day();
        const dayOfWeekEnd = moment(end).day();
        if (dayOfWeekStart !== 0 && dayOfWeekStart !== 6 && dayOfWeekEnd !== 0 && dayOfWeekEnd !== 6) {
            const nextEvents = events.map(existingEvent => {
                return existingEvent.id === event.id
                    ? { ...existingEvent, start, end }
                    : existingEvent
            })

            //Find unit events in period of course event before resizing 
            const unitEventsBetweenBefore = events.filter(existingEvent => {
                return existingEvent.resource === 2
                    && (moment(existingEvent.start).isBetween(event.start, event.end) || moment(existingEvent.start).isSame(event.start))
                    && (moment(existingEvent.end).isBetween(event.start, event.end) || moment(existingEvent.end).isSame(event.end));
            });

            //Find unit events in period of course event after resizing
            const unitEventsBetweenAfter = events.filter(existingEvent => {
                return existingEvent.resource === 2
                    && (moment(existingEvent.start).isBetween(start, end) || moment(existingEvent.start).isSame(start))
                    && (moment(existingEvent.end).isBetween(start, end) || moment(existingEvent.end).isSame(end));
            });

            //Compare to find the affected unit event after resizing
            const differentEvents = difference(unitEventsBetweenBefore, unitEventsBetweenAfter);

            //if there is any affected unit event create a hidden event above it then update event
            if (differentEvents.length > 0) {
                const newEvent = {
                    id: differentEvents[0].id,
                    title: 'New Event',
                    allDay: true,
                    start: differentEvents[0].start,
                    end: differentEvents[0].end,
                    resource: 3,
                }
                const id = differentEvents[0].id + 1;
                const newUpdatedEvent = { ...differentEvents[0], id }
                nextEvents.splice(differentEvents[0].id, 1, newEvent, newUpdatedEvent);
                const updatedEvents = this.updateIndex(nextEvents);
                this.setState({
                    events: updatedEvents,
                })
            } else {
                this.setState({
                    events: nextEvents,
                })
            }

            //alert(`${event.title} was resized to ${start}-${end}`)   
        }
    }

    updateIndex = (array) => {
        return array.map((item, index) => {
            return { ...item, id: index }
        })
    }

    // newEvent = (event) => {
    //     let idList = this.state.events.map(a => a.id)
    //     let newId = Math.max(...idList) + 1
    //     let hour = {
    //         id: newId,
    //         title: 'New Event',
    //         allDay: event.slots.length === 1,
    //         start: event.start,
    //         end: event.end,
    //         resource: 3,
    //     }
    //     this.setState({
    //         events: this.state.events.concat([hour]),
    //     })
    // }

    // allDayAccessorEvent = (event) => {
    //     console.log("test");
    // }

    render() {
        const customDayPropGetter = date => {
            if (date.getDay() === 0 || date.getDay() === 6)
                return {
                    className: 'special-day',
                    style: {
                        backgroundColor: '#f2f2f2'
                    },
                }
            else return {}
        }
        const customEventPropGetter = date => {
            if (date.resource === 3)
                return {
                    className: 'hidden-event',
                    style: {
                        // backgroundColor: 'transparent',
                        // color: 'transparent',
                    }
                }
        }
        return (
            <div style={{ height: '100vh' }}>
                <DragAndDropCalendar
                    selectable
                    localizer={BigCalendar.momentLocalizer(moment)}
                    events={this.state.events}
                    onEventDrop={this.moveEvent}
                    resizable
                    onEventResize={this.resizeEvent}
                    // onSelectSlot={this.newEvent}
                    defaultView={BigCalendar.Views.WEEK}
                    dayPropGetter={customDayPropGetter}
                    eventPropGetter={customEventPropGetter}
                    // resources={resources}
                    // resourceIdAccessor="resourceId"
                    // allDayAccessor={this.allDayAccessorEvent}
                    // resourceTitleAccessor="resourceTitle"
                    components={{
                        // you have to pass your custom wrapper here
                        // so that it actually gets used
                        // dateCellWrapper: ColoredDateCellWrapper,
                        toolbar: Toolbar,
                        timeGutterHeader: TimeGutterHeader,
                        // event: EventComponent
                    }}
                    defaultDate={new Date(2019, 3, 12)}
                />
            </div>
        )
    }
}

export default Dnd