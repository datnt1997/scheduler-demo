import React from 'react';

class EventComponent extends React.Component {
    render() {
        return (
            <div>
                <h1>Test 1</h1>
                <h2>Test 2</h2>
            </div>
        );
    }
}

export default EventComponent;